//
//  AppError.swift
//  Errors
//
//  Created by Oyama Nagashima on 23/01/21.
//

import Foundation

typealias ErrorResponse = (_ error: Error?) -> Void

protocol AppMessage {
    var title: String {get}
    var message: String {get}
}

protocol AppError: AppMessage, LocalizedError { }
extension AppError {
    var message: String {
        return errorDescription ?? ""
    }
}

protocol AppMessageWithAction: AppMessage {
    var buttonText: String { get }
    var buttonAction: () -> Void { get }
}

enum GenericMessage: AppMessageWithAction {
    case generic(title: String, message: String, buttonText: String, buttonAction: () -> Void)
    
    var title: String {
        switch self {
        case let .generic(title, _, _, _):
            return title
        }
    }
    
    var message: String {
        switch self {
        case let .generic(_, text, _, _):
            return text
        }
    }
    
    var buttonText: String {
        switch self {
        case let .generic(_, _, buttonText, _):
            return buttonText
        }
    }
    
    var buttonAction: () -> Void {
        switch self {
        case let .generic(_, _, _, buttonAction):
            return buttonAction
        }
    }
}

enum GeneralError: AppError {
    case string(error: String)
    case emptyString(name: String)
    case somethingWrong(inPart: String)
    case errorTryingToSave
    case nilVariable(variable: String)
    case noConnection
    case genericWarning(withText: String)
    case operationInProgress
    case failedToSaveDataOnTextField

    var title: String {
        switch self {
        case .string:
            return "Erro"
        case .emptyString:
            return "Empty String"
        case .somethingWrong:
            return "Um erro inesperado ocorreu"
        case .errorTryingToSave:
            return "Error"
        case .nilVariable:
            return "Erro"
        case .noConnection:
            return "Sem conexão com a internet"
        case .genericWarning:
            return "Aviso"
        default:
            return "Erro"
        }
    }
    
    var errorDescription: String? {
        switch self {
        case let .string(name):
            return name
        case let .emptyString(name):
            return "The String \(name) is empty."
        case let .somethingWrong(inPart):
            return "Error occured in \(inPart)"
        case .errorTryingToSave:
            return "Erro ao salvar dados."
        case let .nilVariable(variable):
            return "A instância \(variable) está nula quando não deveria."
        case .noConnection:
            return "Não há conexão com a internet. Por favor, ative sua conexão para realizar a operação."
        case let .genericWarning(text):
            return text
        case .operationInProgress:
            return "Já existe uma operação em execução. Aguarde a operação atual terminar para começar uma nova."
        case .failedToSaveDataOnTextField:
            return "Não foi possível salvar os dados selecionados no campo de texto."
        }
    }

    /**
     Checks if the given error is a AppError.
     If it is not, this method returns a generic error
     **/
    static func checkForAppError(_ error: Error, msg: String) -> AppError {
        let appError = error as? AppError
        return appError ?? GeneralError.somethingWrong(inPart: msg)
    }
}


