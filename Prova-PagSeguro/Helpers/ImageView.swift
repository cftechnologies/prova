//
//  ImageView.swift
//  Prova-PagSeguro (iOS)
//
//  Created by 4Apps Solutions on 23/01/21.
//

import Foundation
import SwiftUI
import Alamofire

struct ImageView: View {
    @ObservedObject var imageLoader:ImageLoader
    @State var image:UIImage = UIImage()
    @State private var showAlert = false
    var description: String = ""
    
    init(withURL url:String, withDescription description:String) {
        imageLoader = ImageLoader(urlString:url)
        self.description = description
    }

    var body: some View {

        Image(uiImage: image)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .cornerRadius(10)
            .frame(minWidth: 50, idealWidth: 150, minHeight: 50, idealHeight: 150, alignment: .leading)
            .padding(EdgeInsets.init(top: 20, leading: 20, bottom: 20, trailing: 20))
            .onReceive(imageLoader.didChange) { data in
            self.image = UIImage(data: data) ?? UIImage()
                    
        }
    }
}
