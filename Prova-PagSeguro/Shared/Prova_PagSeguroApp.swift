//
//  Prova_PagSeguroApp.swift
//  Shared
//
//  Created by 4Apps Solutions on 23/01/21.
//

import SwiftUI

@main
struct Prova_PagSeguroApp: App {
    
    init(){
        NetworkReachability.shared.startMonitoring()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        NetworkReachability.shared.stopMonitoring()
     }
}
