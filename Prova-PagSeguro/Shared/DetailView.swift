//
//  DetailView.swift
//  Prova-PagSeguro (iOS)
//
//  Created by 4Apps Solutions on 23/01/21.
//

import SwiftUI
import Combine

struct DetailView: View {
    var beer: Beer
    
    var body: some View {
        
        NavigationView{
            ScrollView(showsIndicators: false){
                VStack(alignment: .center){
                    Text(beer.name )
                            .font(.title)
                            .foregroundColor(Color.blue)
                            .lineSpacing(2)
                    ImageView(withURL: beer.imageURL , withDescription: "")
                        Spacer()
                        VStack(alignment: .leading){
                            Text("Tagline : \(beer.name )")
                                .font(.title2)
                                .foregroundColor(Color.blue)
                                .lineSpacing(2)
                            Spacer()
                            Text("Teor alcoólico: \(beer.abv , specifier: "%.2f")%")
                                .font(.title2)
                                .foregroundColor(Color.blue)
                                .lineSpacing(2)
                            Spacer()
                            Text("Descrição : \(beer.welcomeDescription )")
                                .font(.title2)
                                .foregroundColor(Color.blue)
                                .lineSpacing(2)
                            Spacer()
                            Spacer()
                            Spacer()
                            Spacer()
                        }
                        .padding(EdgeInsets.init(top: 0.0, leading: 15.0, bottom: 0.0, trailing: 15.0))
                    }
                
                }

            }
        }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        let beer = try! Beer.fromJSON("beerExample.json") as Beer?
        DetailView(beer: beer!)
    }
}



