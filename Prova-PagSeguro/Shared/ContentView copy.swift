//
//  ContentView.swift
//  Shared
//
//  Created by Oyama Nagashima on 23/01/21.
//

import SwiftUI
import Alamofire

struct ContentView: View {

    @ObservedObject var viewModel: MainScreenViewModel = MainScreenViewModel()
    @State private var selection: Int?
    
    init(){
        viewModel.loadBeerList()
    }
    
    var body: some View {
        NavigationView {
            
            ZStack(){
                VStack{
                    List{
                        mountBeerList()
                    }
                    Spacer()
                    Button(action: {
                        viewModel.loadBeerList()
                    }){
                        Text("Carregar Mais ...")
                    }
                } // Vstack
                .onAppear(){
                    viewModel.monitorNetStatusChange()
                } // onAppear

                if(viewModel.showProgress){
                    HStack{
                        ProgressView("Carregando…")
                    }.frame(width: 200, height: 200, alignment: .center)
                     .shadow(radius: 5)
                     .background(Color.white)
                     .foregroundColor(Color.blue)
                     .cornerRadius(20)
                     .progressViewStyle(CircularProgressViewStyle(tint: Color.blue))
                }
            }// ZStack
            .navigationBarTitle("Cervejas", displayMode: .inline)
            .alert(isPresented: $viewModel.showConnectionAlert) {
                Alert(title: Text("Conexão internet."), message: Text("Verifique sua conexão com a internet"), dismissButton: .default(Text("Ok")))
            }
        } // NavigationView
        
    } // body View

    
    
    private func mountBeerList() -> AnyView {
        return
            AnyView(
                VStack(alignment: .leading){
                    if(viewModel.beerList.count > 0) {
                        ForEach(viewModel.beerList, id: \.id){ beer in
                            HStack(spacing: 10){
                                VStack(alignment: .center, spacing: 4){
                                    Spacer()
                                    Text(beer.name)
                                    ImageView(withURL: beer.imageURL, withDescription: "")
                                    Spacer()
                                }
                                VStack(alignment: .leading,  spacing: 10){
                                    Text(beer.name)
                                        .font(.title)
                                        .foregroundColor(Color.blue)
                                        .lineSpacing(2)
                                    Text("ABV: \(beer.abv, specifier: "%.2f")%")
                                        .font(.title2)
                                        .lineSpacing(10)
                                    Spacer()
                                    Spacer()
                                    Spacer()
                                    HStack{
                                        Spacer()
                                        NavigationLink(destination: DetailView(beerId: beer.id), tag: beer.id, selection: $selection ) {
                                            Button("Detalhes: \(beer.id)") {
                                                self.selection = beer.id
                                            }
                                        }
                                    }
                                }.frame(minWidth: 0,
                                        maxWidth: .infinity,
                                        minHeight: 0,
                                        maxHeight: .infinity,
                                        alignment: .topLeading)
                                .padding(EdgeInsets.init(top: 20, leading: 20, bottom: 20, trailing: 20))
                            } // HStack
                            .background(Color.white)
                            .cornerRadius(10)
                            .shadow(radius: 5)
                            Spacer().frame(height: 20)
                        } // ForEach
                    }
                } // VStack
            ) // AnyView
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

