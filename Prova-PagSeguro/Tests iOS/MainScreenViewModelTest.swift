//
//  MainScreenViewModelTest.swift
//  Tests iOS
//
//  Created by 4Apps Solutions on 24/01/21.
//

import XCTest
@testable import Prova_PagSeguro


class MainScreenViewModelTest: XCTestCase {
    let mainScreenViewModel: MainScreenViewModel = MainScreenViewModel()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testShouldNotLoadBeerList() throws {
        NetworkReachability.shared.stopMonitoring()
        mainScreenViewModel.loadBeerList()
        XCTAssertTrue(mainScreenViewModel.beerList.count == 0)
    }
    
    func testShouldLoadBeerList() throws {
        NetworkReachability.shared.startMonitoring()
        mainScreenViewModel.loadBeerList()
        XCTAssertTrue(mainScreenViewModel.beerList.count == 0)
        NetworkReachability.shared.stopMonitoring()
    }
    
    func testPerformance() throws {
        self.measure {
            mainScreenViewModel.loadBeerList()
        }
    }

}
