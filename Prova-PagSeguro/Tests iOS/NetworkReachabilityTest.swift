//
//  NetworkReachability.swift
//  Tests iOS
//
//  Created by 4Apps Solutions on 24/01/21.
//

import XCTest
@testable import Prova_PagSeguro

class NetworkReachabilityTest: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIsntConnected() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        NetworkReachability.shared.stopMonitoring()
        XCTAssertFalse(NetworkReachability.shared.isConnected)
    }

    func testIsMotoring() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        NetworkReachability.shared.startMonitoring()
        XCTAssertTrue(NetworkReachability.shared.isMonitoring)
        NetworkReachability.shared.stopMonitoring()
    }

    func testIsntMotoring() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        NetworkReachability.shared.startMonitoring()
        NetworkReachability.shared.stopMonitoring()
        XCTAssertFalse(NetworkReachability.shared.isMonitoring)
    }

    func testNetowrkType() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        NetworkReachability.shared.startMonitoring()
        let networkType = NetworkReachability.shared.interfaceType
        XCTAssertFalse(networkType != nil)
        NetworkReachability.shared.stopMonitoring()
    }


    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
