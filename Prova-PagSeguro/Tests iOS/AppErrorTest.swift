//
//  AppErrorTest.swift
//  Tests iOS
//
//  Created by 4Apps Solutions on 24/01/21.
//

import XCTest

class AppErrorTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGeneralError() throws {
        let appError = GeneralError.string(error: "Erro Genérico")
        XCTAssertTrue(appError.title == "Erro")
    }

    func testGeneralEmptyString() throws {
        let appError = GeneralError.emptyString(name: "Erro Genérico")
        XCTAssertTrue(appError.title == "Empty String")
    }

    func testGeneralSomethingWrong() throws {
        let appError = GeneralError.somethingWrong(inPart: "Erro Genérico")
        XCTAssertTrue(appError.title == "Um erro inesperado ocorreu")
    }
    
    func testErrorTryingToSave() throws {
        let appError = GeneralError.errorTryingToSave
        XCTAssertTrue(appError.title == "Error")
    }

    func testErrorNilVariable() throws {
        let appError = GeneralError.nilVariable(variable: "var")
        XCTAssertTrue(appError.title == "Erro")
    }

    func testNoConnection() throws {
        let appError = GeneralError.noConnection
        XCTAssertTrue(appError.title == "Sem conexão com a internet")
    }

    func testGeneralDescriptionError() throws {
        let appError = GeneralError.string(error: "Erro Genérico")
        XCTAssertTrue(appError.errorDescription == "Erro Genérico")
    }

    func testGeneralEmptyStringDescription() throws {
        let appError = GeneralError.emptyString(name: "Argumento")
        XCTAssertTrue(appError.errorDescription == "The String Argumento is empty.")
    }

    func testGeneralSomethingWrongDescription() throws {
        let appError = GeneralError.somethingWrong(inPart: "Erro")
        XCTAssertTrue(appError.errorDescription == "Error occured in Erro")
    }
    
    func testErrorTryingToSaveDescription() throws {
        let appError = GeneralError.errorTryingToSave
        XCTAssertTrue(appError.errorDescription == "Erro ao salvar dados.")
    }

    func testErrorNilVariableDescription() throws {
        let appError = GeneralError.nilVariable(variable: "var")
        XCTAssertTrue(appError.errorDescription == "A instância var está nula quando não deveria.")
    }

    func testNoConnectionDescription() throws {
        let appError = GeneralError.noConnection
        XCTAssertTrue(appError.errorDescription == "Não há conexão com a internet. Por favor, ative sua conexão para realizar a operação.")
    }
    
    func testOperationInProgress() throws {
        let appError = GeneralError.operationInProgress
        XCTAssertTrue(appError.errorDescription == "Já existe uma operação em execução. Aguarde a operação atual terminar para começar uma nova.")
    }
    
    func testFailedToSaveDataOnTextField() throws {
        let appError = GeneralError.failedToSaveDataOnTextField
        XCTAssertTrue(appError.errorDescription == "Não foi possível salvar os dados selecionados no campo de texto.")
    }

    func testGenericWarning() throws {
        let appError = GeneralError.genericWarning(withText: "Texto de erro.")
        XCTAssertTrue(appError.errorDescription == "Texto de erro.")
    }

    
    func testGenericMessage() throws {
        let appError = GenericMessage.generic(title: "Titulo", message: "Mensagem", buttonText: "Button Text"){}
        XCTAssertTrue(appError.title == "Titulo")
        XCTAssertTrue(appError.message == "Mensagem")
        XCTAssertTrue(appError.buttonText == "Button Text")
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
