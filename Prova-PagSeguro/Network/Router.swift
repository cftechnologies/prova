//
//  Router.swift
//  Network
//
//  Created by Oyama Nagashima on 23/01/21.
//

import Foundation
import Alamofire
import Reachability

enum Router: URLRequestConvertible {

    case listBeers(page: Int, pageSize: Int)
    case beerDetail(beerId: Int)

    var method: HTTPMethod {
        switch self {
        case .listBeers, .beerDetail:
            return .get
        }
    }

    var headers: [String: String]?{
        return ["Content-Type" : "application/json"]
    }
    
    var path: String {
        switch self {
        case let .listBeers(page, pageSize):
            return "beers?page=\(page)&per_page=\(pageSize)"
        case let .beerDetail(beerId):
            return "beers/\(beerId)"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.baseUrlString.asURL()
        let urlPath = url.appendingPathComponent(path).absoluteString.removingPercentEncoding!
        var urlRequest = URLRequest(url: URL(string: urlPath)!)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = self.headers

        urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        urlRequest.timeoutInterval = 10
        return urlRequest
    }
    
}
