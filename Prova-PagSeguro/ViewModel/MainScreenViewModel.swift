//
//  MainScreenViewModel.swift
//  ViewModel
//
//  Created by Oyama Nagashima on 23/01/21.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


class MainScreenViewModel: BaseViewModel {
    
    var beerList: [Beer] = [Beer]()
    private var currentPage: Int = 0

    private func loadBeers(completion: @escaping (AppError?) -> Void) {
        
        currentPage += 1
        
        let url = Router.listBeers(page: currentPage, pageSize: 5)
        
        AF.request(url)
            .validate(contentType: ["application/json"])
            .responseString{(response: DataResponse<String, AFError>)  in
                guard let serverResponse = response.response else {
                    completion(GeneralError.string(error: "Erro Genérico"))
                    return
                }
                if serverResponse.statusCode <= 299 {
                    let jsonData = Data(response.value!.utf8)
                    let tempList = try! JSONDecoder().decode([Beer].self, from: jsonData)
                    self.beerList.append(contentsOf: tempList)
                    completion(nil)
                } else if (response.response?.statusCode ?? 0) > 399 {
                    completion(GeneralError.string(error: "Erro ao recuperar dados"))
                }
        }

    }
    
    public func loadBeerList(){
        if(NetworkReachability.shared.isConnected){
            self.showProgress = true
            self.loadBeers(){error in
                if(error != nil){
                    print(error?.message ?? "Erro ao sincronizar.")
                }
                self.showProgress = false
            }
        } else {
            self.connectionMessage = "Verifique sua conexão com a Internet!"
            self.showProgress = false
            self.showConnectionAlert = true
        }
    }
    
    


}
