//
//  BaseViewModel.swift
//  ViewModel
//
//  Created by Oyama Nagashima on 23/01/21.
//

import Foundation
import Combine

class BaseViewModel: ObservableObject{
    
    let appQueue: DispatchQueue
    var isDebug = false
    @Published var showProgress = false
    @Published var connectionMessage: String = ""
    @Published var showConnectionAlert: Bool = false
    @Published var errorMessage: String = ""
    
    
    init() {
        appQueue = DispatchQueue(label: "br.com.pagseguro.sync")
        #if DEBUG
        isDebug = true
        #endif
    }
    
    public func monitorNetStatusChange(){
        NetworkReachability.shared.netStatusChangeHandler = {
            DispatchQueue.main.async {
                self.showConnectionAlert = !NetworkReachability.shared.isConnected
            }
        }
    }
    
}
