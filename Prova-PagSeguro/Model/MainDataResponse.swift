//
//  MainDataResponse.swift
//  Model
//
//  Created by Oyama Nagashima on 23/01/21.
//

import Foundation
import ObjectMapper


class MainDataResponse: Mappable{
    var beers: [Beer] = []

    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        beers <- map[""]
    }

    
}
